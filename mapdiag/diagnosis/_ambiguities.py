# Copyright 2024, Université de technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#                 CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Contains stuff related to finding the ambiguitues in the association matrix
"""

import itertools

import numpy.typing as npt


def row_cluster(A: npt.NDArray):  # pylint: disable=invalid-name
    """Find the row to row clusters of the bipartite graph represented by A

    Parameters
    ----------
    A : ndarray
        The association matrix of shape ``(n, m)`` of the bipartite graph.
        The element ``(i, j)`` of the matrix is 1 if `i` and `j` are linked, 0 otherwise

    Returns
    -------
    List
        A list of `n` frozensets. The frozenset at index `i`:
        - Is empty if ith row is not associated to any column
        - contains only `i` if the row forms a unique cluster with potential multiple columns
        - contains the row number and the number of the rows in the same cluster otherwise.

    """
    row_to_col = [frozenset(i for i, v in enumerate(x) if v) for x in A]
    col_to_row = [frozenset(i for i, v in enumerate(x) if v) for x in A.T]
    row_to_row = [
        frozenset(itertools.chain.from_iterable(col_to_row[c] for c in r))
        for r in row_to_col
    ]

    crit = sum(len(r) for r in row_to_row)
    for _ in itertools.count():
        # print(f"it={it}, crit={crit}")
        row_to_row = [
            frozenset(itertools.chain.from_iterable(row_to_row[x] for x in r))
            for r in row_to_row
        ]
        old_crit, crit = crit, sum(len(r) for r in row_to_row)
        if old_crit == crit:
            break

    return row_to_row
