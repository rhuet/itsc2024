""" Decision that can me taken by the diagnosis
"""

from enum import Enum


class DiagnosisDecision(Enum):
    """Possible decisions of a diagnosis framework"""

    WELL_MAPPED = 1
    NOT_EXISTING = 2
    MISSING_MAP = 3
    UNKNOWN = 4
