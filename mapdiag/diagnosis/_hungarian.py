# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Hugarian diagnosis method

The hungarian diagnosis method forces association with an Hungarian algorithm.
It thus uses only the most probable association between the diagnosed map and the
reference map.

"""

import numpy as np
import numpy.typing as npt

from scipy.optimize import linear_sum_assignment

from mapdiag.diagnosis import DiagnosisDecision


def hungarian_diagnosis(D: npt.NDArray) -> npt.NDArray:  # pylint: disable=invalid-name
    """Compute the hungarian diagnosis using the given distance matrix

    The hungarian algoritm resolves the conflicts by using a linear sum assignment
    algorithm.

    Parameters
    ----------
    D : ndarray
        The distance matrix of the bipartite graph of shape ``(n, m)``.
        Use numpy.finfo(numpy.float64).max to represent an infeasible assigment.

    Returns
    -------
    ndarray
        An array of `DiagnosisDecision` concerning the rows of the association matrix.

    """
    row_ind, col_ind = linear_sum_assignment(D)

    # As D contains max float if the assignment is not feasible,
    # we need to keep only the feasible assigments
    # Then, all the features are considered NOK except if an assignation remains

    # Default everything to NOK
    map_decisions = np.full(D.shape[0], DiagnosisDecision.NOT_EXISTING)
    diag_decisions = np.full(D.shape[1], DiagnosisDecision.MISSING_MAP)

    # Check the associated rows
    for row, col in zip(row_ind, col_ind):
        if D[row, col] < np.finfo(D.dtype).max:
            # The association cost is < float max so association is valid
            map_decisions[row] = DiagnosisDecision.WELL_MAPPED
            diag_decisions[col] = DiagnosisDecision.WELL_MAPPED

    return map_decisions, diag_decisions
