# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" This module contains the proposed diagnosis method, without explicit association.

This method does not use an assignment algorithm, as the greedy one does.
However, it is able to resolve some of the ambiguities, and then takes more
decision that the cautious one.

"""

import numpy as np
import numpy.typing as npt
from scipy.optimize import linear_sum_assignment

from mapdiag.diagnosis import DiagnosisDecision
from mapdiag.diagnosis._ambiguities import row_cluster


def _resolve_ambiguity(
    A: npt.NDArray,  # pylint: disable=invalid-name
    group_idx: npt.NDArray,
    decisions: npt.NDArray,
) -> None:
    """Resolve the given ambiguity case and update decisions accordingly

    It is shown in the paper that our method is able to resolve some ambiguous cases
    and to find some OK elements in the clusters.
    In this function, we try to find certain elements of a group that are always associated
    whatever the association used is. For the other elements, the UNKNOWN desicion is taken.

    Parameters
    ----------
    A : ndarray
        The association matrix
    group_idx : ndarray
        The indices of the rows in A that are part of the cluster to solve.
    decisions : ndarray
        The array of decisions for the whole case.
        This array is modified inplace with the decisions taken for the group

    """

    # Get the columns corresponding to the given group_idx
    cols = np.where(np.any(A[group_idx, :], axis=0))[0]

    # Corresponding submatrix of A
    S = A[group_idx[:, None], cols[None, :]]  # pylint: disable=invalid-name

    # Now, treat each line of the submatrix
    for i, l in enumerate(S):
        # extract submatrix of S where cols st. S[i, :] = 1 and remove line i
        s = np.delete(S[:, l], i, axis=0)

        # If s is not square, we make it square by adding rows of 0 (than cannot take a col)
        # or cols of 1 (that can bind to every col).
        # This is because we just want to chech if remaining rows can "take" every column,
        # but we don't care if all rows are associated
        d = s.shape[1] - s.shape[0]
        if d > 0:
            # We need to add d rows of zeros
            new_rows = np.zeros((d, s.shape[1]), dtype=bool)
            s = np.vstack((s, new_rows))
        elif d < 0:
            # We need to add -d cols
            new_cols = np.ones((s.shape[0], -d), dtype=bool)
            s = np.hstack((s, new_cols))

        # Now, we check if it is possible to construct an assigment
        try:
            with np.errstate(divide="ignore"):
                linear_sum_assignment(-np.log(s))
            # Assigment worked, so feature i is ambiguous
            decisions[group_idx[i]] = DiagnosisDecision.UNKNOWN
        except ValueError:
            # Assigment is not possible, so map feature i is a true positive
            decisions[group_idx[i]] = DiagnosisDecision.WELL_MAPPED


def our_diagnosis(A: npt.NDArray) -> npt.NDArray:  # pylint: disable=invalid-name
    """Our diagnosis using the given association matrix

    Parameters
    ----------
    A : ndarray
        The association matrix of shape ``(n, m)`` of the bipartite graph.
        The element ``(i, j)`` of the matrix is 1 if `i` and `j` are linked, 0 otherwise

    Returns
    -------
    ndarray
        An array of `DiagnosisDecision` concerning the rows of the association matrix.

    """
    row2row = row_cluster(A)
    not_associated_idx = [i for i, v in enumerate(row2row) if not v]
    uniquely_associated_idx = [i for i, v in enumerate(row2row) if len(v) == 1]
    ambiguous_groups = set(v for v in row2row if len(v) > 1)

    map_decisions = np.empty((A.shape[0],), dtype=DiagnosisDecision)
    map_decisions[not_associated_idx] = DiagnosisDecision.NOT_EXISTING
    map_decisions[uniquely_associated_idx] = DiagnosisDecision.WELL_MAPPED

    # Solve the ambiguous cases
    for group in ambiguous_groups:
        group_idx = np.array(list(group))
        _resolve_ambiguity(A, group_idx, map_decisions)

    A = A.T

    row2row = row_cluster(A)
    not_associated_idx = [i for i, v in enumerate(row2row) if not v]
    uniquely_associated_idx = [i for i, v in enumerate(row2row) if len(v) == 1]
    ambiguous_groups = set(v for v in row2row if len(v) > 1)

    diag_decisions = np.empty((A.shape[0],), dtype=DiagnosisDecision)
    diag_decisions[not_associated_idx] = DiagnosisDecision.MISSING_MAP
    diag_decisions[uniquely_associated_idx] = DiagnosisDecision.WELL_MAPPED

    # Solve the ambiguous cases
    for group in ambiguous_groups:
        group_idx = np.array(list(group))
        _resolve_ambiguity(A, group_idx, diag_decisions)

    return map_decisions, diag_decisions
