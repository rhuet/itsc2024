""" Evaluation of the diagnosis
"""

from dataclasses import dataclass

from mapdiag.diagnosis import DiagnosisDecision


@dataclass
class ErrorsCount:
    """Counts the different types of errors"""

    e1: int
    e2: int
    e4: int
    e5: int


# pylint: disable=too-many-arguments
def evaluate(
    map_decisions, diag_decisions, world_decisions, map_gt, diag_gt, world_gt
) -> ErrorsCount:
    """Evaluates the decisions of the diagnosis from the ground truth

    Parameters
    ----------
    map_decisions: ndarray
        Decusisions that where taken about the map to diagnose
    diag_decisions: ndarray
        Decisions that where taken about the map that is given as a diagnosis (eg. from a SLAM),
        used to find the missing elements in the diagnosed map
    world_decisions: ndarray
        Does only have a meaning in simulation where we can now if an element of the world
        is in the map or not
    map_gt: ndarray
        Ground truth about the generation of the map
    diag_gt: ndarray
        The good classification of the diagnosis elements
    world_gt: ndarray
        The good classification of the world elements

    Returns
    -------
    ErrorsCount
        The count of each type of errors
    """
    counts = ErrorsCount(0, 0, 0, 0)

    # Iterate on the map to find e1 and e2
    for i, map_decision in enumerate(map_decisions):
        if (
            map_decision == DiagnosisDecision.WELL_MAPPED
            and map_gt[i] == DiagnosisDecision.NOT_EXISTING
        ):
            counts.e1 += 1

        elif (
            map_decision == DiagnosisDecision.NOT_EXISTING
            and map_gt[i] == DiagnosisDecision.WELL_MAPPED
        ):
            counts.e2 += 1

    # Iterate on the diag_decisions to find e4
    # e4 is if feature in
    for i, diag_decision in enumerate(diag_decisions):
        if (
            diag_decision == DiagnosisDecision.MISSING_MAP
            and diag_gt[i] == DiagnosisDecision.NOT_EXISTING
        ):
            counts.e4 += 1

    # Iterate over the world dectisions to find e5
    # e5 is if world_decision is ok when gt is not
    for i, world_decision in enumerate(world_decisions):
        if (
            world_decision == DiagnosisDecision.NOT_EXISTING
            and world_gt[i] == DiagnosisDecision.MISSING_MAP
        ):
            counts.e5 += 1

    return counts
