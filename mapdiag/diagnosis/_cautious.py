# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""Cautious diagnosis method

The cautious diagnosis method does not use an assignment algorithm as the Greedy one does.
Instead, always output "Unknown" for any element in an ambiguity cluster.

"""

import numpy as np
import numpy.typing as npt

from mapdiag.diagnosis._ambiguities import row_cluster
from mapdiag.diagnosis import DiagnosisDecision


def cautious_diagnosis(A: npt.NDArray) -> npt.NDArray:  # pylint: disable=invalid-name
    """Cautious diagnosis using the given association matrix

    Parameters
    ----------
    A : ndarray
        The association matrix of shape ``(n, m)`` of the bipartite graph.
        The element ``(i, j)`` of the matrix is 1 if `i` and `j` are linked, 0 otherwise

    Returns
    -------
    ndarray
        An array of `DiagnosisDecision` concerning the rows of the association matrix.

    """
    row2row = row_cluster(A)
    not_associated_idx = [i for i, v in enumerate(row2row) if not v]
    uniquely_associated_idx = [
        i for i, v in enumerate(row2row) if len(v) == 1 and A[i, :].sum() == 1
    ]

    map_decisions = np.full((A.shape[0],), DiagnosisDecision.UNKNOWN)
    map_decisions[not_associated_idx] = DiagnosisDecision.NOT_EXISTING
    map_decisions[uniquely_associated_idx] = DiagnosisDecision.WELL_MAPPED

    #### From diagnosis
    A = A.T
    row2row = row_cluster(A)
    not_associated_idx = [i for i, v in enumerate(row2row) if not v]
    uniquely_associated_idx = [
        i for i, v in enumerate(row2row) if len(v) == 1 and A[i, :].sum() == 1
    ]

    diag_decisions = np.full(A.shape[0], DiagnosisDecision.UNKNOWN)
    diag_decisions[not_associated_idx] = DiagnosisDecision.MISSING_MAP
    diag_decisions[not_associated_idx] = DiagnosisDecision.WELL_MAPPED

    return map_decisions, diag_decisions
