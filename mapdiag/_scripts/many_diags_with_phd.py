# pylint: skip-file

import pickle

import numpy as np

from multiprocessing import Pool
from .single_with_phd import main as simu


def main():
    n_simus = 1_000
    # Draw seeds
    seed = 816974029
    print(f"Using seed {seed}")
    seeds_gen = np.random.default_rng(seed)
    seeds = seeds_gen.integers(np.iinfo(int).max, size=n_simus)

    with Pool() as p:
        res = p.map(simu, seeds)

    with open(f"res-phd-{seed}.pkl", "wb") as f:
        pickle.dump(res, f)
