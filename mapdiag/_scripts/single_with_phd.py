# pylint: skip-file

"""Test script of the PHD
"""

from dataclasses import dataclass

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from mapdiag.noise import (
    PoissonUniformClutter,
    RangedPoissonUniformClutter,
    CenteredGaussianNoise,
    UniformMissDetection,
    generate_observation,
    generate_ranged_observation,
)

from mapdiag.diagnosis import (
    hungarian_diagnosis,
    cautious_diagnosis,
    our_diagnosis,
    evaluate,
    DiagnosisDecision,
)

from mapdiag import generate_trajectory
from mapdiag.filtering import GMPHDFilter


@dataclass
class Res:
    nsp: int
    e1: int
    e2: int


def main(seed=None):  # pylint: disable=missing-function-docstring
    world_size = 200
    density = 1 / 10**2

    n_features = int(density * world_size**2)

    print(f"Generating a world with {n_features} features")

    # Initialize generator
    rng = np.random.default_rng(seed)
    world = rng.uniform(high=world_size, size=(n_features, 2))

    # The map is an observation of the world with given noise params
    map_detection_proba = 0.95  # 5% missdetection
    map_std = 0.5
    map_noise_cov = (map_std**2) * np.eye(2)
    map_clutter_intensity = 0.05 * n_features  # 5% clutter

    association_threshold = 1

    map_clutter_model = PoissonUniformClutter(
        map_clutter_intensity, world_size, rng=rng
    )
    map_missdetection_model = UniformMissDetection(map_detection_proba, rng=rng)
    map_noise_model = CenteredGaussianNoise(map_noise_cov, rng=rng)

    map_, world_gt, map_gt = generate_observation(
        world,
        map_clutter_model,
        map_noise_model,
        map_missdetection_model,
        threshold_for_gt=association_threshold,
        rng=rng,
    )

    # Generate a trajectory inside the world
    traj = generate_trajectory(2.5, world_size, 250, rng=rng)

    # plt.scatter(*world.T)
    # plt.scatter(*map_.T)
    # plt.scatter(*traj.T)
    # plt.show()

    obs_range = 50
    obs_detection_proba = 0.95
    obs_std = 0.2
    obs_noise_cov = (obs_std**2) * np.eye(2)
    obs_clutter_intensity = 2

    # Initialize the filter
    filter_ = GMPHDFilter(0.95, obs_detection_proba, 0.001, obs_noise_cov)

    # Initialize noise generators for obs
    obs_clutter_model = RangedPoissonUniformClutter(
        obs_clutter_intensity, world_size, obs_range, np.empty((2,)), rng=rng
    )
    obs_noise_model = CenteredGaussianNoise(obs_noise_cov, rng=rng)
    obs_missdetection_model = UniformMissDetection(obs_detection_proba, rng=rng)

    for position in tqdm(traj):
        obs_clutter_model.center = position

        observations, _, _ = generate_ranged_observation(
            world,
            position,
            obs_range,
            obs_clutter_model,
            obs_noise_model,
            obs_missdetection_model,
            rng=rng,
        )

        # filter_.predict(position, obs_range)
        filter_.update(observations, position, obs_range, obs_clutter_model)
        filter_.reduce()
        filter_.birth(observations)
        filter_._check()  # pylint: disable=protected-access

    # Retrieve map and world (and gt) that were **observable** from the trajectory
    # Bacause we cannot diagnose what we cannot see or course
    observable_map_idx = _observable(map_, traj, obs_range)
    observable_world_idx = _observable(world, traj, obs_range)

    observable_map = map_[observable_map_idx]
    observable_map_gt = map_gt[observable_map_idx]

    observable_world = world[observable_world_idx]
    observable_world_gt = world_gt[observable_world_idx]

    ##### DIAGNOSIS #####

    # Extract estimate from filter
    estimates = filter_.estimates[1]

    diff = estimates[None, :, :] - observable_map[:, None, :]
    d2 = np.einsum("ijk,ijk->ij", diff, diff)
    A = d2 < association_threshold**2

    no = np.array([])

    ####################
    ## Hungarian Diag ##
    ####################

    D = np.sqrt(d2)  # pylint: disable=invalid-name
    D[D > association_threshold] = np.finfo(np.float64).max

    hung_map_dec, _ = hungarian_diagnosis(D)
    n_nsp_hungarian = (hung_map_dec == DiagnosisDecision.UNKNOWN).sum()

    eval_hungarian = evaluate(hung_map_dec, no, no, observable_map_gt, no, no)

    res_hungarian = Res(
        n_nsp_hungarian,
        eval_hungarian.e1,
        eval_hungarian.e2,
    )

    ###################
    ## Cautious Diag ##
    ###################

    cautious_map_dec, _ = cautious_diagnosis(A)
    n_nsp_cautious = (cautious_map_dec == DiagnosisDecision.UNKNOWN).sum()

    eval_cautious = evaluate(
        cautious_map_dec,
        no,
        no,
        observable_map_gt,
        no,
        no,
    )

    res_cautious = Res(n_nsp_cautious, eval_cautious.e1, eval_cautious.e2)

    # print(f"Cuatious : {res_cautious}")

    ##############
    ## Our Diag ##
    ##############

    our_map_dec, _ = our_diagnosis(A)
    n_nsp_ours = (our_map_dec == DiagnosisDecision.UNKNOWN).sum()

    eval_ours = evaluate(our_map_dec, no, no, observable_map_gt, no, no)
    # print(f"Ours : {res_ours}")
    res_ours = Res(n_nsp_ours, eval_ours.e1, eval_ours.e2)

    return (
        res_hungarian,
        res_cautious,
        res_ours,
        observable_world.shape[0],
        filter_.n_estimates,
        observable_map_gt,
        observable_world_gt,
    )


def _observable(points, traj, range_):
    diff = points[None, :, :] - traj[:, None, :]
    d2 = np.einsum("ijk,ijk->ij", diff, diff)

    return np.any(d2 < range_**2, axis=0)


if __name__ == "__main__":
    main()
