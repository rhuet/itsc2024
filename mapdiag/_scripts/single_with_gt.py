# pylint: disable=[too-many-locals,missing-function-docstring]

""" Test script
"""

from dataclasses import dataclass

import numpy as np

# import matplotlib.pyplot as plt

from mapdiag.noise import (
    PoissonUniformClutter,
    CenteredGaussianNoise,
    UniformMissDetection,
    generate_observation,
)

from mapdiag.diagnosis import (
    cautious_diagnosis,
    hungarian_diagnosis,
    our_diagnosis,
    evaluate,
    DiagnosisDecision,
)


@dataclass
class Res:
    nsp: int
    e1: int
    e2: int
    e4: int
    e5: int


def main(seed=None):  # pylint: disable=all
    world_size = 200
    density = 4 / 10**2

    n_features = int(density * world_size**2)

    # print(f"Generating a world with {n_features} features")

    # Initialize generator
    rng = np.random.default_rng(seed)
    world = rng.uniform(high=world_size, size=(n_features, 2))

    # The map is an observation of the world with given noise params
    map_detection_proba = 0.95  # 5% missdetection
    map_std = 0.5
    map_noise_cov = (map_std**2) * np.eye(2)
    map_clutter_intensity = 0.05 * n_features  # 5% clutter

    association_threshold = 1

    map_clutter_model = PoissonUniformClutter(
        map_clutter_intensity, world_size, rng=rng
    )
    map_missdetection_model = UniformMissDetection(map_detection_proba, rng=rng)
    map_noise_model = CenteredGaussianNoise(map_noise_cov, rng=rng)

    map_, world_gt, map_gt = generate_observation(
        world,
        map_clutter_model,
        map_noise_model,
        map_missdetection_model,
        threshold_for_gt=association_threshold,
        rng=rng,
    )

    # plt.scatter(*world.T)
    # plt.scatter(*map_.T)
    # plt.show()

    # Do the diagnosis of the map using the ground truth (the world)

    diff = world[None, :, :] - map_[:, None, :]
    d2 = np.einsum("ijk,ijk->ij", diff, diff)
    A = d2 < association_threshold**2  # pylint: disable=invalid-name

    ####################
    ## Hungarian Diag ##
    ####################

    D = np.sqrt(d2)  # pylint: disable=invalid-name
    D[D > association_threshold] = np.finfo(np.float64).max

    hung_map_dec, hung_diag_dec = hungarian_diagnosis(D)
    n_nsp_hungarian = (hung_map_dec == DiagnosisDecision.UNKNOWN).sum()

    eval_hungarian = evaluate(
        hung_map_dec, hung_diag_dec, hung_diag_dec, map_gt, world_gt, world_gt
    )

    res_hungarian = Res(
        n_nsp_hungarian,
        eval_hungarian.e1,
        eval_hungarian.e2,
        eval_hungarian.e4,
        eval_hungarian.e5,
    )

    ###################
    ## Cautious Diag ##
    ###################

    cautious_map_dec, cautious_diag_dec = cautious_diagnosis(A)
    n_nsp_cautious = (cautious_map_dec == DiagnosisDecision.UNKNOWN).sum()

    eval_cautious = evaluate(
        cautious_map_dec,
        cautious_diag_dec,
        cautious_diag_dec,
        map_gt,
        world_gt,
        world_gt,
    )

    res_cautious = Res(
        n_nsp_cautious,
        eval_cautious.e1,
        eval_cautious.e2,
        eval_cautious.e4,
        eval_cautious.e5,
    )

    # print(f"Cuatious : {res_cautious}")

    ##############
    ## Our Diag ##
    ##############

    our_map_dec, our_diag_dec = our_diagnosis(A)
    n_nsp_ours = (our_map_dec == DiagnosisDecision.UNKNOWN).sum()

    eval_ours = evaluate(
        our_map_dec, our_diag_dec, our_diag_dec, map_gt, world_gt, world_gt
    )
    # print(f"Ours : {res_ours}")
    res_ours = Res(n_nsp_ours, eval_ours.e1, eval_ours.e2, eval_ours.e4, eval_ours.e5)

    return res_hungarian, res_cautious, res_ours, map_gt, world_gt


if __name__ == "__main__":
    main()
