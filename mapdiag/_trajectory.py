# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Trajectory generation
"""

import numpy as np


def generate_trajectory(speed: float, size: int, n_points: int, *, rng=None):
    """Generate Trajectory points inside the world

    The spacing between the points is almost `speed` (bacause a point is drawn at each time unit)
    but with some noise added.
    The direction of the trajectory is the same from one point to the next one, except for some
    non-biased noise.
    If the trajectory reaches a border of the world, it "bounces" on that border.

    Parameters
    ----------
    speed: float
        The speed at which the elements moves, used to compute spacing between points in
        distance unit per time unit. A point is drawn for each time unit.
    size: int
        Specifies the size of the world, such that the trajectory points are always in the square
        ``(0, 0), (size, size)``.
    n_points: int
        Number of points to generate.
    rng: Generator, optional
        The random generator to use. If None is provided, then a generator is created with
        ``numpy.random.default_rng()``.

    Returns
    -------
    ndarray
        The generated trajectory points, of shape (n_points, 2)
    """
    if rng is None:
        rng = np.random.default_rng()

    # Peek a first random point in the world
    points = rng.uniform(0, size, (1, 2))

    # Use an angle to avoid brutal changes of direction
    angle = rng.uniform(0, 2 * np.pi)

    for _ in range(n_points - 1):
        # Draw a random distance
        distance = rng.normal(speed, 0.2 * speed)

        # Compute the next point
        next_point = points[-1] + distance * np.array([np.cos(angle), np.sin(angle)])

        # If the next point is outside the world, change the angle
        while np.any(next_point < 0) or np.any(next_point > size):
            angle = rng.uniform(0, 2 * np.pi)
            next_point = points[-1] + distance * np.array(
                [np.cos(angle), np.sin(angle)]
            )

        # Add next point to the trajectory
        next_point = next_point.reshape((1, 2))
        points = np.vstack((points, next_point))

        # Add slight noise to the angle
        angle += rng.normal(0, 0.1)

    return points
