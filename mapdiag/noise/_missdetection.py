# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Contains classes used for missdetection of points
"""

from abc import ABC, abstractmethod
from typing import Generator

import numpy as np
import numpy.typing as npt


class MissDetectionModel(ABC):
    """Abstract Base Class for the generation of missdetections"""

    @abstractmethod
    def missdetect(self, points: npt.NDArray) -> npt.NDArray:
        """Apply missdetection model to the given list of points

        Parameters
        ----------
        points : ndarray of shape (n_points, dim)
            The array of points to which apply the missdetection model.
            The array is not modified by the function.

        Returns
        -------
        ndarray
            The given list of points with some missed.
            The points are missed according to the base class and its parameters.
        """

    @abstractmethod
    def is_detected(self, points: npt.NDArray) -> npt.NDArray:
        """Apply missdetection model, but unlike ``missdetect`` it returns an
        array of shape ``(n_points,)`` with true / false values

        Parameters
        ----------
        points : ndarray of shape (n_points, dim)
            The array of points to which apply the missdetection model.
            The array is not modified by the function.

        Returns
        -------
        ndarray
            An array of shape (n_points,) telling whether the feature is
            detected or not
        """

    @abstractmethod
    def detection_proba(self, points: npt.ArrayLike) -> npt.NDArray:
        """Get the detection probability for each point of an array

        Parameters
        ----------
        points : array_like of shape (dim,) or (n_points, dim)
            The point or the points where the detection probability is computed.

        Returns
        -------
        ndarray of shape (n_points,)
            The detection probability of each point given as input.

        """


class NoMissDetection(MissDetectionModel):
    """A model where all the elements are always detected"""

    def missdetect(self, points: npt.NDArray) -> npt.NDArray:
        """Apply missdetection model to the given list of points

        Parameters
        ----------
        points : ndarray of shape (n_points, dim)
            The array of points to which apply the missdetection model.
            The array is not modified by the function.

        Returns
        -------
        ndarray
            The same array as the input.
        """
        return points

    def is_detected(self, points: npt.NDArray) -> npt.NDArray:
        """Apply missdetection model, but unlike ``missdetect`` it returns an
        array of shape ``(n_points,)`` with true / false values

        Parameters
        ----------
        points : ndarray of shape (n_points, dim)
            The array of points to which apply the missdetection model.
            The array is not modified by the function.

        Returns
        -------
        ndarray
            An array of shape (n_points,) telling whether the feature is
            detected or not
        """
        return np.ones(points.shape[0])

    def detection_proba(self, points: npt.ArrayLike) -> npt.NDArray:
        """Get the detection probability for each point of an array

        Parameters
        ----------
        points : array_like of shape (dim,) or (n_points, dim)
            The point or the points where the detection probability is computed.

        Returns
        -------
        ndarray
            The detection probability of each point given as input (1 for each point),
            of shape (n_points,)

        """
        return np.ones(points.shape[-1], dtype=float)


class UniformMissDetection(MissDetectionModel):
    """A model where all points have the same probability to be detected"""

    def __init__(self, detection_probability: float, *, rng: Generator = None):
        self.detection_probability = detection_probability

        if rng is None:
            rng = np.random.default_rng()

        self.rng = rng

    def missdetect(self, points: npt.NDArray) -> npt.NDArray:
        """Apply missdetection model to the given list of points

        Parameters
        ----------
        points : ndarray of shape (n_points, dim)
            The array of points to which apply the missdetection model.
            The array is not modified by the function.

        Returns
        -------
        ndarray
            The given points with some missed given the `detection_probability` param.
        """
        num_features = points.shape[0]
        detections = self.rng.choice(
            [True, False],
            size=num_features,
            p=[self.detection_probability, 1 - self.detection_probability],
        )
        return points[detections]

    def is_detected(self, points: npt.NDArray) -> npt.NDArray:
        """Apply missdetection model, but unlike ``missdetect`` it returns an
        array of shape ``(n_points,)`` with true / false values

        Parameters
        ----------
        points : ndarray of shape (n_points, dim)
            The array of points to which apply the missdetection model.
            The array is not modified by the function.

        Returns
        -------
        ndarray
            An array of shape (n_points,) telling whether the feature is
            detected or not
        """
        vals = self.rng.uniform(size=points.shape[0])
        return vals < self.detection_probability

    def detection_proba(self, points: npt.ArrayLike) -> npt.NDArray:
        """Get the detection probability for each point of an array

        Parameters
        ----------
        points : array_like of shape (dim,) or (n_points, dim)
            The point or the points where the detection probability is computed.

        Returns
        -------
        ndarray
            The detection probability of each point given as input,
            of shape (n_points,)

        """
        return self.detection_probability * np.ones(points.shape[-1], dtype=float)
