# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Contains the function used for the generation of an observation
"""


from typing import Generator, NamedTuple

import numpy as np
import numpy.typing as npt

from mapdiag.noise import ClutterModel, NoiseModel, MissDetectionModel
from mapdiag.diagnosis import DiagnosisDecision


class ObservationWithGT(NamedTuple):
    """Result of an observation with ground truth about how it was generated"""

    observation: npt.NDArray
    world_gt: npt.NDArray  # Wheter each element is in the obs or not
    obs_gt: npt.NDArray  # Whether the elements is from an existing one or not (clutter)


def generate_ranged_observation(
    points: npt.NDArray, position: npt.NDArray, range_: float, *args, **kwargs
):
    """Generates observations with limited ragne FOV

    Parameters
    ----------
    points: ndarray
        The points for which to generate the observations, of shape (dim,) for a single point
        or (n_points, dim) for multiple points.
    position: ndarray
        The position of the observator, used to compute which points are observable
    range_: float
        The range of the observator, used to compute which points are observable
    *args
        Passed to generate_observations
    **kwargs
        Passed to generate_observations

    Returns
    -------
    ObservationWithGT
        see ``generate_observation`` for more details
    """
    # Keep only points that are in range
    diff = points - position
    d2 = np.einsum("ij,ij->i", diff, diff)
    mask = d2 < range_**2

    points = points[mask]
    return generate_observation(points, *args, **kwargs)


# pylint: disable=[too-many-locals,too-many-arguments]
def generate_observation(
    points: npt.NDArray,
    clutter_model: ClutterModel,
    noise_model: NoiseModel,
    missdetection_model: MissDetectionModel,
    *,
    threshold_for_gt: float = None,
    rng: Generator = None,
) -> ObservationWithGT:
    """Generate an observation using the given models

    Parameters
    ----------
    points: ndarray
        The points for which to generate the observations, of shape (dim,) for a single point
        or (n_points, dim) for multiple points.
    clutter_model : ClutterModel
    noise_model: NoiseModel
    missdetection_model: MissDetectionModel
    threshold_for_gt: float, optional
        If the threshold is set, then an observation with distance > threshold to the point that was
        used to generate it is set to be a false positive (NOK in obs_gt). Moreover, the associated
        point is also set to be missed (False in world_gt)
    rng: Generator, optional
        The random generator to use. If none is provided, numpy.random.default_generator()
        is used.

    Returns
    -------
    ObservationWithGT
        1) The observation of the given `points` input with missdetections noise and clutter
        2) The ground truth concerning the points (whether a point is in the detections or not)
        3) The ground truth concernint the observations (whether they correspond to a point
           or to clutter)
    """
    points = np.atleast_2d(points)
    if rng is None:
        rng = np.random.default_rng()

    # Determine wich points are detected or not
    is_detected = missdetection_model.is_detected(points)
    detected_points = points[is_detected, :]

    # Fist step of world gt: points that are not detected are NOK
    world_gt = np.where(
        is_detected, DiagnosisDecision.WELL_MAPPED, DiagnosisDecision.MISSING_MAP
    )

    # First step of obs_gt: detected_points lead to OK
    obs_gt = np.full(detected_points.shape[0], DiagnosisDecision.WELL_MAPPED)

    # Add noise to the points
    noisy = noise_model.add_noise(detected_points)

    # If threshold is set and the distance from detected to noisy is more than threshold,
    # then both world_gt and obs_gt go NOK
    if threshold_for_gt is not None:
        diff = noisy - detected_points
        d2 = np.einsum("ij,ij->i", diff, diff)
        mask = d2 > threshold_for_gt**2

        # Astuce because we just touch to the detected ones here
        world_gt_detected = world_gt[is_detected]
        world_gt_detected[mask] = DiagnosisDecision.MISSING_MAP
        world_gt[is_detected] = world_gt_detected

        obs_gt[mask] = DiagnosisDecision.NOT_EXISTING

    # Generate clutter
    clutter = clutter_model.generate()
    clutter_gt = np.full(clutter.shape[0], DiagnosisDecision.NOT_EXISTING)

    # Merge clutter and noisy data / gt
    res = np.vstack((noisy, clutter))
    res_gt = np.concatenate((obs_gt, clutter_gt))

    # Shuffle observations
    indices = np.arange(res.shape[0])
    rng.shuffle(indices)

    return ObservationWithGT(res[indices], world_gt, res_gt[indices])
