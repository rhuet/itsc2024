# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Contains classes to generate clutter points

The provided classes are used to generate the maps and the observations.

"""


from abc import ABC, abstractmethod
from typing import Generator

import numpy as np
import numpy.typing as npt


class ClutterModel(ABC):
    """Abstract Base Class for the generation of clutter"""

    @abstractmethod
    def generate(self) -> npt.NDArray:
        """Generate clutter

        This method must be specialized in the sublasses to match with the model and
        its params.

        """

    @abstractmethod
    def intensity_at(self, points: npt.ArrayLike) -> npt.ArrayLike:
        """Value of the intensity function of the clutter at the given points.

        Parameters
        ----------
        points : numpy.typing.ArrayLike
            The points at which the intensity function of the clutter is to be evaluated.
            The function can be evaluated at a single point (of shape ``(d,)``) or
            for a list of points (of shape ``(n, d)``) where `d` is the dimension of a point
            and `n` the number of points.

        Returns
        -------
        numpy.typing.ArrayLike
            The value of the intensity at the requested points.

        """


class NoClutter(ClutterModel):
    """A clutter generator that never generates any clutter"""

    def generate(self) -> npt.NDArray:
        """Generate clutter

        For this generator, no clutter is generated

        Returns
        -------
        numpy.typing.NDArray
            An array of shape ``(0, d)`` (with no element).

        """
        return np.ndarray([], (0, 2))

    def intensity_at(self, points: npt.ArrayLike) -> npt.ArrayLike:
        """Value of the intensity function of the clutter at the given points.

        For this generator, it is always 0.

        Parameters
        ----------
        points : numpy.typing.ArrayLike
            The points at which the intensity function of the clutter is to be evaluated.
            For this generator, only the shape is used as the clutter intensity is 0 over
            the whole space.

        Returns
        -------
        numpy.typing.ArrayLike
            An array of zeros like `points`.

        """
        return np.zeros_like(points)


class PoissonUniformClutter(ClutterModel):
    """Generate a pisson-distributed number of uniformly-distributed clutter.

    This clutter model has an `intensity` param, that is used to parametrize the
    expected number of clutter.
    Once the number of clutter is generated, the elements are drawn from a random distribution.

    """

    def __init__(self, intensity: float, size: int, *, rng: Generator = None) -> None:
        self.intensity = intensity
        self.size = size

        if rng is None:
            rng = np.random.default_rng()

        self.generator = rng

    def generate(self) -> npt.NDArray:
        """Generate clutter

        See the class documentation for more detail.

        Returns
        -------
        numpy.typing.NDArray
            An array of shape ``(n_clutter, d)`` of randomly generated clutter.

        """
        n_clutter = self.generator.poisson(self.intensity)
        clutter = self.generator.uniform(0, self.size, (n_clutter, 2))

        return clutter

    def intensity_at(self, points: npt.ArrayLike) -> npt.ArrayLike:
        """Value of the intensity function of the clutter at the given points.

        The value is 0 if the point is outside of the bounds of the generator.
        Otherwise, it corresponds to the pdf of the uniform law.

        Parameters
        ----------
        points : numpy.typing.ArrayLike
            The points at which the intensity function of the clutter is to be evaluated.
            The points may be inside or outside the bounds of the generator

        Returns
        -------
        numpy.typing.ArrayLike
            The intensity function evaluated at all the points, as an array of shape ``(n,)``
            where `n` is the number of points.

        """
        inbounds = np.logical_and(
            np.all(points >= 0.0, axis=1), np.all(points <= self.size, axis=1)
        )
        return np.where(inbounds, 1 / self.size**2, 0)


class RangedPoissonUniformClutter(ClutterModel):
    """Poisson number of clutter, uniform dist in polar"""

    # pylint: disable=too-many-arguments
    def __init__(
        self,
        intensity: int,
        size: int,
        range_: float,
        position: np.ndarray,
        *,
        rng=None,
    ) -> None:
        if rng is None:
            rng = np.random.default_rng()

        self.intensity = intensity
        self.size = size
        self.range = range_
        self.center = position
        self.rng = rng

    def generate(self) -> npt.NDArray:
        """Generate clutter

        See the class documentation for more detail.

        Returns
        -------
        numpy.typing.NDArray
            An array of shape ``(n_clutter, d)`` of randomly generated clutter.

        """
        n_clutter = self.rng.poisson(self.intensity)

        # Generate clutter in polar coordinates to respect the range
        clutter = self.rng.uniform([0, 0], [self.range, 2 * np.pi], (n_clutter, 2))

        # Convert to cartesian coordinates
        cartesian = np.array(
            [[r * np.cos(theta), r * np.sin(theta)] for r, theta in clutter]
        )

        # Translate to the center
        if clutter.size > 0:
            clutter = cartesian + self.center

        # Remove clutter outside the world
        clutter = clutter[
            np.all(clutter > 0, axis=1) & np.all(clutter < self.size, axis=1)
        ]

        return clutter

    def intensity_at(self, points: npt.ArrayLike) -> npt.ArrayLike:
        """Value of the intensity function of the clutter at the given points.

        The value is 0 if the point is outside of the bounds of the generator.
        Otherwise, it corresponds to the pdf of the uniform law.

        Parameters
        ----------
        points : numpy.typing.ArrayLike
            The points at which the intensity function of the clutter is to be evaluated.
            The points may be inside or outside the bounds of the generator

        Returns
        -------
        numpy.typing.ArrayLike
            The intensity function evaluated at all the points, as an array of shape ``(n,)``
            where `n` is the number of points.

        """
        points = np.atleast_2d(points)

        # Compute d2 from position
        diff = points - self.center
        d2 = np.einsum("ij,ij->i", diff, diff)

        inrange = d2 < self.range**2
        inworld = np.logical_and(
            np.all(points >= 0.0, axis=1), np.all(points <= self.size, axis=1)
        )

        inbounds = np.logical_and(inrange, inworld)

        # Technically, the area for the clutter is the circle of radius range, minus the part
        # of that area that is not in the world, but we simplify as the area of the circle
        val = self.intensity / (np.pi * self.range**2)

        return np.where(inbounds, val, 0)
