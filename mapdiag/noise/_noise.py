# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Contains methods and classes to generate noise on a list of points.

The provided classes are used to generate the maps and the observations.

"""

from abc import ABC, abstractmethod
from typing import Generator

import numpy as np
import numpy.typing as npt


class NoiseModel(ABC):  # pylint: disable=too-few-public-methods
    """Abstract Base class fot the generation of noise on a list of points"""

    @abstractmethod
    def add_noise(self, points: npt.NDArray) -> npt.NDArray:
        """Add noise to the given points

        Parameters
        ----------
        points: 2-D ndarray of shape ``(n_points, dim)``
            The points to which the noise will be added.
            The array is not modified, instead a new array is returned.

        Returns
        -------
        ndarray
            The points given as parameter with some noise added to them.
            The generated noise depends on the subclass and its parameters.
            The shape of the returned array is the same as the shape of `points`.

        """


class CenteredGaussianNoise(NoiseModel):  # pylint: disable=too-few-public-methods
    """Add centered gaussian noise to the points.

    The covariance matrix of the noise is the same for all the points.
    """

    def __init__(self, cov: npt.ArrayLike, *, rng: Generator = None) -> None:
        self.cov = cov

        if rng is None:
            rng = np.random.default_rng()

        self.rng = rng

    def add_noise(self, points: npt.NDArray) -> npt.NDArray:
        """Add gaussian centered noise to the given points

        Parameters
        ----------
        points: 2-D ndarray of shape ``(n_points, dim)``
            The points to which the noise will be added.
            `dim` must be the same as the dimension of the cov matrix.

        Returns
        -------
        ndarray
            The given points with centered gaussian noise added.
        """
        (n, d) = points.shape
        noise = self.rng.multivariate_normal(np.zeros(d), self.cov, n)

        return points + noise
