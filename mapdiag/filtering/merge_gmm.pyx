# Copyright 2024, université de technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# distutils: language = c++
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True

from libc.math cimport log, INFINITY

import numpy as np


cdef inline double det2_sym_upper(double[:,:] matrix) noexcept nogil:
    if matrix.shape[0] == 1:
        return matrix[0,0]
    if matrix.shape[0] == 2:
        return matrix[0,0] * matrix[1,1]-matrix[0,1] * matrix[0,1]
    if matrix.shape[0] == 3:
        return (matrix[0,0] * matrix[1,1] * matrix[2,2]
                - matrix[0,0] * matrix[1,2] * matrix[1,2]
                - matrix[0,1] * matrix[0,1] * matrix[2,2]
                + 2 * matrix[0,1] * matrix[0,2] * matrix[1,2]
                - matrix[0,2] * matrix[0,2] * matrix[1,1])
    return 0.

cdef inline double kl_bound_loss_of_a_single_merge(
    double[:,:] buffer,
    Py_ssize_t i,
    Py_ssize_t j,
    double[:] weights,
    double[:,:] locs,
    double[:,:,:] covs,
    double[:] plogdet,
) noexcept nogil:
    cdef double weights_sum = weights[i]+weights[j]
    cdef Py_ssize_t k,l
    for k in range(covs.shape[1]):
        for l in range(k,covs.shape[1]):
            buffer[k,l] = (
                weights[i] * covs[i,k,l]
                + weights[j] * covs[j,k,l]
                + (
                    weights[i]
                    * weights[j]
                    * (locs[i,k]-locs[j,k])
                    * (locs[i,l]-locs[j,l])
                    / weights_sum
                )
            )/weights_sum
    cdef double det_cov_merge = det2_sym_upper(buffer)
    cdef double logdet_cov_merge = log(det_cov_merge)
    return .5*(weights_sum*logdet_cov_merge - plogdet[i] - plogdet[j])

cdef extern from * nogil:
    """
    class Merge {
        public:
            double kl_bound_loss;
            Py_ssize_t component1;
            Py_ssize_t component2;
            Merge() {}
            Merge(const double& kl, const Py_ssize_t& comp1, const Py_ssize_t& comp2)
                : kl_bound_loss(kl), component1(comp1), component2(comp2) {}
            bool operator<(const Merge& oth) const {return this->kl_bound_loss>oth.kl_bound_loss;}
    };
    """
    cdef cppclass Merge:
        double kl_bound_loss
        Py_ssize_t component1
        Py_ssize_t component2
        Merge()
        Merge(double kl, Py_ssize_t comp1, Py_ssize_t comp2)

cdef extern from "<algorithm>" namespace "std" nogil:
    cdef void make_heap[RandomIt](RandomIt first, RandomIt last)
    cdef void pop_heap[RandomIt](RandomIt first, RandomIt last)
    cdef void push_heap[RandomIt](RandomIt first, RandomIt last)

from libcpp.list cimport list as cpplist
from libcpp.vector cimport vector
from libcpp.map cimport map as cppmap
from cython.operator import dereference, preincrement, address

def merge(
    double[:] weights,
    double[:,:] locs,
    double[:,:,:] covs,
    min_components_number = None,
    max_components_number = None,
    kl_bound_threshold = None
):
    cdef Py_ssize_t min_comp
    if min_components_number is None:
        min_comp = 1
    else:
        min_comp = min_components_number
    cdef Py_ssize_t max_comp
    if max_components_number is None:
        max_comp = weights.shape[0]
    else:
        max_comp = max_components_number
    cdef double threshold = 0.
    if kl_bound_threshold is not None:
        threshold = kl_bound_threshold

    cdef Py_ssize_t i=0,j=0
    cdef cppmap[Py_ssize_t,vector[Merge]] queues
    cdef cppmap[Py_ssize_t,vector[Merge]].iterator queues_iterator
    cdef vector[Merge]* current_queue
    cdef Merge* best_merge
    cdef Merge* current_merge

    cdef Py_ssize_t n = weights.shape[0]
    cdef Py_ssize_t d = locs.shape[1]

    cdef double[:] ext_weights = np.zeros(2*n)
    cdef double[:,:] ext_locs = np.zeros((2*n,d))
    cdef double[:,:,:] ext_covs = np.zeros((2*n,d,d))
    cdef unsigned char[:] valid = np.zeros(2*n, dtype=np.uint8)
    cdef double[:] plogdet = np.zeros(2*n)

    cdef double[:,:] buffer = np.zeros((d,d))
    cdef double cost
    costs = np.zeros((n,))
    cdef double[:] costs_view = costs

    cdef Py_ssize_t ncomponents = n
    cdef Py_ssize_t new_idx = n
    cdef double current_cost = 0
    cdef double cum_cost = 0

    cdef bint best_valid = 0
    #with nogil:
    if True:
        for i in range(n):
            ext_weights[i] = weights[i]
            for k in range(d):
                ext_locs[i,k] = locs[i,k]
            for k in range(d):
                for l in range(k,d):
                    ext_covs[i,k,l] = covs[i,k,l]
            valid[i] = 1
            plogdet[i] = ext_weights[i]*log(det2_sym_upper(ext_covs[i]))

        for i in range(n):
            queues[i] = vector[Merge]()
            current_queue = address(queues[i])
            current_queue.reserve(i+1)
            for j in range(0,i):
                cost = kl_bound_loss_of_a_single_merge(
                    buffer, i, j, ext_weights, ext_locs, ext_covs,
                    plogdet,
                )
                current_queue.push_back(Merge(cost, i, j))
            make_heap(current_queue.begin(),current_queue.end())

        while True:
            best_valid = False
            queues_iterator = queues.begin()
            best_merge = address(dereference(queues_iterator).second.front())
            while queues_iterator != queues.end():
                current_queue = address(dereference(queues_iterator).second)
                while not current_queue.empty():
                    if valid[current_queue.front().component2]:
                        break
                    pop_heap(current_queue.begin(),current_queue.end())
                    current_queue.pop_back()
                if not current_queue.empty():
                    current_merge = address(current_queue.front())
                    if best_valid:
                        if current_merge.kl_bound_loss < best_merge.kl_bound_loss:
                            best_merge = current_merge
                    else:
                        best_merge = current_merge
                        best_valid = True
                preincrement(queues_iterator)

            if not best_valid:
                break
            i = best_merge.component1
            j = best_merge.component2
            current_cost = best_merge.kl_bound_loss

            if ncomponents == min_comp:
                break

            if cum_cost+current_cost > threshold and ncomponents <= max_comp:
                break

            cum_cost += current_cost
            costs_view[new_idx-n] = cum_cost
            # new point
            ext_weights[new_idx] = ext_weights[i]+ext_weights[j]
            for k in range(d):
                ext_locs[new_idx,k] = (ext_weights[i]*ext_locs[i,k] +
                                       ext_weights[j]*ext_locs[j,k])/ext_weights[new_idx]
            for k in range(d):
                for l in range(k,d):
                    ext_covs[new_idx,k,l] = (
                        ext_weights[i] * ext_covs[i,k,l]
                        + ext_weights[j] * ext_covs[j,k,l]
                        + (
                            ext_weights[i]
                            * ext_weights[j]
                            * (ext_locs[i,k]-ext_locs[j,k])
                            * (ext_locs[i,l]-ext_locs[j,l])
                            / ext_weights[new_idx]
                        )
                    )/ext_weights[new_idx]
            valid[new_idx]=1
            valid[i]=0
            valid[j]=0
            queues.erase(i)
            queues.erase(j)
            ncomponents-=1

            plogdet[new_idx] = ext_weights[new_idx]*log(det2_sym_upper(ext_covs[new_idx]))
            queues[new_idx]=vector[Merge]()
            current_queue = address(queues[new_idx])
            current_queue.reserve(2*n-new_idx+1)
            for i in range(new_idx):
                if valid[i]:
                    cost = kl_bound_loss_of_a_single_merge(
                        buffer, i, new_idx, ext_weights, ext_locs, ext_covs,
                        plogdet,
                    )
                    current_queue.push_back(Merge(cost, new_idx, i))

            make_heap(current_queue.begin(), current_queue.end())
            new_idx+=1

    ret_weights = np.zeros(ncomponents)
    ret_locs = np.zeros((ncomponents,d))
    ret_covs = np.zeros((ncomponents,d,d))

    cdef double[:] ret_weights_view = ret_weights
    cdef double[:,:] ret_locs_view = ret_locs
    cdef double[:,:,:] ret_covs_view = ret_covs

    cdef Py_ssize_t ret_idx = 0
    for i in range(new_idx):
        if valid[i]:
            ret_weights_view[ret_idx] = ext_weights[i]
            for k in range(d):
                ret_locs_view[ret_idx,k] = ext_locs[i,k]
            for k in range(d):
                for l in range(k,d):
                    ret_covs_view[ret_idx,k,l] = ext_covs[i,k,l]
                    if l>k:
                        ret_covs_view[ret_idx,l,k] = ext_covs[i,k,l]
            ret_idx += 1
    return (costs[:(new_idx-n)], ret_weights, ret_locs, ret_covs)
