# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# pylint: disable=invalid-name

""" Functions used to create a kalman filter
"""

import numpy as np
import numpy.typing as npt


def S_(P: npt.ArrayLike, H: npt.ArrayLike, R: npt.ArrayLike) -> npt.NDArray:
    """Innovation covariance matrix
    Parameters
    ----------
    P : array_like
        The covariance of the prediction
    H : array_like
        The observation jacobian matrix
    R : array_like
        The covariance of the observation

    Returns
    -------
    ndarray
        The covariance of the innovation

    """
    return H @ P @ H.T + R


def K_(P: npt.ArrayLike, H: npt.ArrayLike, S: npt.ArrayLike) -> npt.NDArray:
    """Kalman gain
    Parameters
    ----------
    P : array_like
        The covariance of the prediction
    H : array_like
        The observation jacobian matrix
    S : array_like
        The covariance of the innovation

    Returns
    -------
    ndarray
        The Kalman gain

    """
    return P @ H @ np.linalg.inv(S)


def P_(
    P: npt.ArrayLike, H: npt.ArrayLike, K: npt.ArrayLike, R: npt.ArrayLike
) -> npt.NDArray:
    """Updated covariance
    Parameters
    ----------
    P : array_like
        The covariance of the prediction
    H : array_like
        The observation jacobian matrix
    K : array_like
        The Kalman gain
    R : array_like
        The coariance of the obseration

    Returns
    -------
    ndarray
        The updated coariance

    """
    I = np.zeros(P.shape)
    idx = np.arange(I.shape[-1])
    I[..., idx, idx] = 1
    IKH = I - K @ H

    # Use swapaxes to transpose only the last 2 dims without knowing the number of dims
    return IKH @ P @ IKH.swapaxes(-1, -2) + K @ R @ K.swapaxes(-1, -2)
