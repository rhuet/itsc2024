# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# pylint: disable=[too-many-instance-attributes,too-many-arguments,too-many-locals]

""" Gaussian Mixture Probability Hypothesis Density Filter

This filter is simplified for the requirements of the application, and thus contains neither
observation nor evolution models.
"""

from typing import Tuple

import numpy as np
import numpy.typing as npt
from scipy.special import logsumexp

from mapdiag.noise import ClutterModel
from mapdiag.filtering._gaussian import logmvnpdf
from mapdiag.filtering._kalman import S_, K_, P_

# pylint: disable=import-error,no-name-in-module
from mapdiag.filtering.merge_gmm import merge


class GMPHDFilter:
    """The Gaussian Mixture PHD filter

    Simplified to match only the requirements of the application, does
    not allows other evolution / observation model, also restrained to use 2-d hypotheses
    However, it is able to deal with simple distance-based field of view
    """

    def __init__(
        self,
        ps: float,
        pd: float,
        bw: float,
        obs_cov: npt.NDArray,
        process_cov: npt.NDArray = None,
    ) -> None:
        """
        Parameters
        ----------
        ps: float
            Probability of survival of elements.
            Only applied in predict for elements that are in range of the sensor
        pd: float
            Probability of detection of an element that is in range
            Used in update
        bw: float
            Birth weight, the weight to be assigned to an hypothesis
            the first time it is created
        obs_cov: ndarray
            The covariance matrix of the observation process
        process_cov: ndarray, optional
            Covariance of the prediction step.
            Only applied in predict for elements that are in range of the sensor.
            If None, then predict step does not do anything
        """

        # Gaussian Mixture Hypotheses and weights (replicated by ro properties)
        self._log_weights = np.empty((0,), dtype=float)
        self._hypotheses_means = np.empty((0, 2), dtype=float)
        self._hypotheses_covs = np.empty((0, 2, 2), dtype=float)

        ## Parameters of the filter that can be overriden ##
        self.log_ps = np.log(ps)
        self.pd = pd
        self.log_bw = np.log(bw)
        self.obs_cov = obs_cov
        self.process_cov = process_cov

        self.log_prune_threshold = np.log(1e-54)  # (value only for numeric stability)
        self.nhyps_per_weight = 5  # ratio n_hyps / total weight for merging
        self.kl_bound_threshold = 1e-2  # for merging

    ### Properties ###

    @property
    def log_weights(self) -> npt.NDArray:
        """Get the weights of the mixture in the log domain

        For numerical stability, the filter only uses log weights, so this
        is just a getter

        Returns
        -------
        ndarray
            The log weights of the hypothesis of the filter
        """
        return self._log_weights

    @property
    def hypotheses_means(self) -> npt.NDArray:
        """Get the means of the gaussian distributions in the mixture

        Returns
        -------
        ndarray
            The means of the hypotheses in the filter of shape (n_hyps, dim)
        """
        return self._hypotheses_means

    @property
    def hypotheses_covs(self) -> npt.NDArray:
        """Get the covariance matrices of the gaussian distribution in the mixture

        Returns
        -------
        ndarray
            The covariance matrices of the hypotheses in ht filter of shape (n_hyps, dim, dim)
        """
        return self._hypotheses_covs

    @property
    def n_hyps(self) -> int:
        """Number of hypotheses in the filter

        Returns
        -------
        int
            The number of hypotheses in the gaussian mixture used by the filter
        """
        return self._log_weights.shape[0]

    @property
    def weights(self) -> npt.NDArray:
        """Get the weights of the mixture in the natural domain

        The filter uses log weihts internaly, so this function computes
        back the corresponding weights. There is no guarantee of numerical stability

        Returns
        -------
        ndarray
            The weights of the mixture
        """
        return np.exp(self._log_weights)

    @property
    def integral(self) -> float:
        """Get the integral of the gaussian mixture

        This is basically just the sum of the weights, so the operation does not
        have big cost other than coputing the weights from the log weights.

        Returns
        -------
        float
            The integral of the underlying distribution
        """
        return self.weights.sum()

    @property
    def n_estimates(self) -> int:
        """Get the number of targets estimated by the PHD function

        This result is only the casting of the integral to an int to get
        an integer number of elements

        Returns
        -------
        int
            The estimated number of targets
        """
        return int(self.integral)

    @property
    def estimates(self) -> Tuple[npt.NDArray, npt.NDArray, npt.NDArray]:
        """Get the estimated targets of the filter

        This function extract targets, and gives all the corresponding parameters of the gaussian

        Returns
        -------
        tuple(ndarray, ndarray, ndarray)
            A tuple containing the weights, means and covs of the gaussian extracted as esimates
            Tho get point estimates, simply use the second ndarray of the tuple
        """
        n = self.n_estimates
        idx = np.argsort(self.log_weights)
        # Keep biggest weights
        keep = idx[-n:]

        return (
            self.weights[keep],
            self.hypotheses_means[keep],
            self.hypotheses_covs[keep],
        )

    def predict(self, obs_position: npt.ArrayLike, range_: float):
        """Predict the hypotheses of the filter

        As there is no prediction model, this just allows for the addition of some covariance to the
        hypotheses that are in range of the observator and the diminution of the weight

        Parameters
        ----------
        obs_position: ndarray
            position of the observator, used to compute which hypotheses to update
            of shape (dim,)
        range_: float
            range of the observator, used to compute chich hypotheses to update
        """

        if self.process_cov is None:
            return

        # Compute hypotheses that are in range
        diff = self.hypotheses_means - obs_position
        d2 = np.einsum("ij,ij -> i", diff, diff)
        in_range = d2 < range_**2

        self._log_weights[in_range] += self.log_ps
        self._hypotheses_covs[in_range] += self.process_cov

    # pylint: disable=invalid-name
    def update(
        self,
        measurements: npt.NDArray,
        observator_position: np.ndarray,
        range_: float,
        clutter_model: ClutterModel,
    ) -> None:
        """Perform the update step of the filter

        Parameters
        ----------
        measurements: ndarray
            Array of measurements of shape (n_meas, dim)
        observator_position: ndarray
            Position of the observator used to compute which features should be detected
        range_: float
            Range of the observator used to compute chich features shoud be detected
        clutter_model: ClutterModel
            The clutter model of the observator

        """
        if self._log_weights.shape[0] == 0:
            return

        # Convenient aliases for more concise code
        states = self._hypotheses_means
        covs = self._hypotheses_covs

        # Pre-compute values that do not depent from observations
        H = np.eye(2)  # Only this model is used for now in the filter

        z_hat = (H @ states.T).T
        S = S_(covs, H, self.obs_cov)
        K = K_(covs, H, S)
        P = P_(covs, H, K, self.obs_cov)

        # Pre-allocate output
        n_hyps = self._log_weights.shape[0]
        n_meas = measurements.shape[0]
        n_upd = n_hyps * (n_meas + 1)
        upd_weights = np.empty(n_upd)
        upd_hyps_means = np.empty((n_upd, 2))
        upd_hyps_covs = np.empty((n_upd, 2, 2))

        pd = np.full_like(self._log_weights, self.pd)
        # Compute distances between observator and hypotheses
        diff = states - observator_position
        d2 = np.einsum("ij,ij -> i", diff, diff)
        pd[d2 > range_**2] = 0

        # Ignore warning from expected log(0) = -Inf
        with np.errstate(divide="ignore"):
            logpd = np.log(pd)
            log1mpd = np.log(1 - pd)

        # Create missdetection hypotheses
        upd_weights[0:n_hyps] = log1mpd + self.log_weights
        upd_hyps_means[0:n_hyps] = self.hypotheses_means
        upd_hyps_covs[0:n_hyps] = self.hypotheses_covs

        # Loop on every measurement and hypothesis
        for i, meas in enumerate(measurements):
            unnormalized_log_weights = np.empty(n_hyps)
            for h, hyp in enumerate(self.hypotheses_means):
                index = n_hyps * (i + 1) + h

                upd_hyps_means[index] = hyp + K[h] @ (meas - z_hat[h])
                upd_hyps_covs[index] = P[h]

                unnormalized_log_weights[h] = (
                    logpd[h] + self.log_weights[h] + logmvnpdf(meas, z_hat[h], S[h])
                )

            # Normalization factor
            # Add lc to array
            with np.errstate(divide="ignore"):
                d = np.append(
                    unnormalized_log_weights, np.log(clutter_model.intensity_at(meas))
                )
            # Compute log-denominator using logsumexp
            d = logsumexp(d)
            # Normalize log-weights and store
            begin = n_hyps * (i + 1)
            end = n_hyps * (i + 2)  # not included
            upd_weights[begin:end] = unnormalized_log_weights - d

        self._log_weights = upd_weights
        self._hypotheses_means = upd_hyps_means
        self._hypotheses_covs = upd_hyps_covs

    def reduce(self):
        """Reduction of the number of hypotheses in the filter

        This function differs from habitual reduction strategies of PHDs as pruning
        is used only for numeric stability, and only merging is used.
        The merging strategy is the one of ``mapdiag.filtering.merge_gmm`` which is based on [1].

        The merging keeps at most ``self.n_hyps_per_weight * self.integral`` elements, and more
        merges are done until the total cost does not exceed ``self.kl_bound_threshold``.

        References
        ----------

        [1] A. R. Runnalls, "Kullback-Leibler Approach to Gaussian Mixture Reduction",
        IEEE Transactions on Aerospace and Electronic Systems,
        vol. 43, nᵒ 3, p. 989‑999, juill. 2007, doi: 10.1109/TAES.2007.4383588.
        """
        # Prune hypotheses with to small weight
        mask = self._log_weights > self.log_prune_threshold

        ws = self.weights[mask]
        locs = self._hypotheses_means[mask]
        covs = self._hypotheses_covs[mask]

        max_components = int(self.nhyps_per_weight * ws.sum())
        max_components = max(max_components, 100)

        _, ws, locs, covs = merge(
            ws,
            locs,
            covs,
            max_components_number=max_components,
            kl_bound_threshold=self.kl_bound_threshold,
        )

        self._log_weights = np.log(ws)
        self._hypotheses_means = locs
        self._hypotheses_covs = covs

    def birth(self, observations: npt.NDArray):
        """Birth model of the filter

        Adds hypotheses where previous measurements were

        Parameters
        ----------
        observations: ndarray
            Observations for which to generate birth components
        """
        birth_cov = self.obs_cov[None, ...]

        self._log_weights = np.concatenate(
            (self._log_weights, self.log_bw * np.ones(observations.shape[0]))
        )

        self._hypotheses_means = np.concatenate(
            (self._hypotheses_means, observations), axis=0
        )
        self._hypotheses_covs = np.concatenate(
            (
                self._hypotheses_covs,
                np.repeat(birth_cov, observations.shape[0], axis=0),
            ),
            axis=0,
        )

    def _check(self):
        """Some checks on the filter, debug function only

        This function runs some checks on the filter integrity, namely:
        - That the shapes of the log_weights, means and covs are coherent
        - That all the covs are definite-positive

        THIS FUNCTION SHOULD NOT BE USED IN PRODUCTION
        """
        assert self._hypotheses_covs.shape[0] == self.hypotheses_means.shape[0]
        assert self._hypotheses_means.shape[0] == self._log_weights.shape[0]

        np.linalg.cholesky(self.hypotheses_covs)
