# Copyright 2024, CNRS (Heudiasyc), France,
#                 Rémy Huet <remy.huet@hds.utc.fr>

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Contains usefull function defs for gaussian / multivariate normal manipulation
"""

import numpy as np
import numpy.typing as npt

from numba import njit


@njit(cache=True)
def squared_mahalanobis(
    x: npt.ArrayLike, mean: npt.ArrayLike, cov: npt.ArrayLike
) -> float:
    """Compute the squared mahalanobis distance between a point and a distribution

    Parameters
    ----------
    x: array_like
        The point at which to evaluate the distance, of shape (d,)
    mean: array_like
        The mean of the gaussian distribusion, of shape (d,)
    cov: array_like
        The covariance of the gaussian distribution, of shape (d, d)

    Returns
    -------
    float
        The squared mahalanobis distance between x and the distribution
        N(mean, cov)
    """
    err = x - mean
    # No need to .T because err is a vector of shape (d,).
    # solve(cov, err) is equal to inv(cov) @ err
    return err @ np.linalg.solve(cov, err)


@njit(cache=True)
def logmvnpdf(x: npt.ArrayLike, mean: npt.ArrayLike, cov: npt.ArrayLike) -> float:
    """Return the log of the pdf of a multivariate-normal dist (mean, cov) at (x)

    This function is much more optimized tan coputing the log of a mvnpdf

    Parameters
    ----------
    x: array_like
        The point at which to evaluate the distance, of shape (d,)
    mean: array_like
        The mean of the gaussian distribusion, of shape (d,)
    cov: array_like
        The covariance of the gaussian distribution, of shape (d, d)

    Returns
    -------
    float
        The log of the pdf of the multivariate normal N(mean, cov)
        evaluated at x

    """
    d = cov.shape[0]
    return (-1 / 2) * (
        d * np.log(2 * np.pi)
        + np.linalg.slogdet(cov)[0]
        + squared_mahalanobis(x, mean, cov)
    )
