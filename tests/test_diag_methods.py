# pylint: disable=[too-many-locals,duplicate-code]

""" Test script
"""

import numpy as np

# import matplotlib.pyplot as plt

from mapdiag.diagnosis import (
    cautious_diagnosis,
    hungarian_diagnosis,
    our_diagnosis,
    DiagnosisDecision,
)


def test_associations():  # pylint: disable=missing-function-docstring
    map_ = []
    world = []
    # (1,2) ambiguity
    map_.append([0, 0])
    world.append([0, 0.2])
    world.append([0, -0.4])
    # (2,1) ambiguity
    map_.append([2, 0])
    map_.append([2, 1])
    world.append([2, 0.2])
    # (2,2) ambiguity
    # 1 0
    # 1 1
    map_.append([4, 0])
    map_.append([4, 1])
    world.append([4, 0.2])
    world.append([4, 1.2])
    # (2,2) ambiguity
    # 1 1
    # 1 1
    map_.append([6, 0])
    map_.append([6, 1])
    world.append([6, 0.2])
    world.append([6, 0.6])

    map_ = np.array(map_)
    world = np.array(world)

    hung_gt = np.array(
        [
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.NOT_EXISTING,
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.WELL_MAPPED,
        ]
    )
    cautious_gt = np.array(
        [
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
        ]
    )
    our_gt = np.array(
        [
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.UNKNOWN,
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.WELL_MAPPED,
            DiagnosisDecision.WELL_MAPPED,
        ]
    )

    # plt.scatter(*world.T)
    # plt.scatter(*map_.T)
    # plt.show()

    # Do the diagnosis of the map using the ground truth (the world)
    association_threshold = 1

    diff = world[None, :, :] - map_[:, None, :]
    d2 = np.einsum("ijk,ijk->ij", diff, diff)
    A = d2 < association_threshold**2  # pylint: disable=invalid-name

    ####################
    ## Hungarian Diag ##
    ####################

    D = d2  # pylint: disable=invalid-name
    D[D > association_threshold**2] = np.finfo(np.float64).max

    hung_dec, _ = hungarian_diagnosis(D)
    assert (hung_dec == hung_gt).all()
    print("Hungarian: OK")

    ###################
    ## Cautious Diag ##
    ###################

    cautious_dec, _ = cautious_diagnosis(A)
    assert (cautious_dec == cautious_gt).all()
    print("Cautious: OK")

    ##############
    ## Our Diag ##
    ##############

    our_dec, _ = our_diagnosis(A)
    assert (our_dec == our_gt).all()
    print("Ours: OK")
