# pylint: skip-file
import numpy as np

from mapdiag.noise import PoissonUniformClutter


def test_uniform_clutter():
    model = PoissonUniformClutter(4, 15)

    clutter = model.generate()
    assert clutter.shape[1] == 2
    points = np.array([[0, 1], [2, 6.4], [-1, 3], [4, 64]])

    val = 1 / 15**2
    values = np.array([val, val, 0, 0])

    intensities = model.intensity_at(points)
    assert intensities.shape == (values.shape[0],)
    assert (intensities == values).all()
