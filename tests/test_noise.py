# pylint: skip-file
import numpy as np

from mapdiag.noise import CenteredGaussianNoise


def test_gaussian_noise():
    m = np.random.random(size=(2, 3))
    cov = m @ m.T
    model = CenteredGaussianNoise(cov)

    points = np.random.random(size=(100, 2))
    res = model.add_noise(points)

    assert res.shape == points.shape
