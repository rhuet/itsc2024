{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default";
    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, devenv, systems, ... } @ inputs:
    let
      forEachSystem = nixpkgs.lib.genAttrs (import systems);
    in
    {
      packages = forEachSystem (system: {
        devenv-up = self.devShells.${system}.default.config.procfileScript;
      });

      devShells = forEachSystem
        (system:
          let
            pkgs = nixpkgs.legacyPackages.${system};
            myPython311 = pkgs.python3.withPackages (ps: with ps; [
              pyqt6
              build
            ]);
          in
          {
            default = devenv.lib.mkShell {
              inherit inputs pkgs;
              modules = [
                {
                  packages = [
                    pkgs.libgcc # Required as a dependecy of pandas
                    pkgs.zlib # Required dependency of numpy
                    #pkgs.binutils
                    pkgs.gcc
                    #pkgs.glibc DOESN'T BUILD WITH GLIBC
                    pkgs.kdePackages.qtbase

                    pkgs.pre-commit
                    pkgs.python3Packages.tkinter
                    # pkgs.pylint
                    ### Other supported python versions
                    pkgs.python39
                    pkgs.python310
                    pkgs.python312
                  ];

                  languages.python = {
                    enable = true;
                    package = myPython311;
                    venv = {
                      enable = true;
                    };
                  };
                }
              ];
            };
          });
    };
}
