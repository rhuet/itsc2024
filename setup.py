# pylint: skip-file
from setuptools import setup, Extension
from Cython.Build import cythonize

setup(
    ext_modules=cythonize(
        [
            Extension(
                name="mapdiag.filtering.merge_gmm",
                sources=["mapdiag/filtering/merge_gmm.pyx"],
            )
        ]
    ),
)
