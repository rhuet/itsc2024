# Codes for ITSC 2024 paper

## Reproducing the results from the paper

- Install the package. As the repo may have been updated, you can make sure that you have the exact same version by cloning this repo and checkout to version 1.0.0, then `pip install .`.
- Get the results, two ways:
    - Launch the commands `mapdiag-many-gt` and `mapdiag-many-phd`. The seed that was used to get the results (randomly chosen) is already written in the files. WARNING: the first program took 90 cpu hours, the second ??? when I launched them. Make sure to lauchn them in a machine with enough cores to get the results in a decent time.
    - If you don't want to lauch the calculation, you can retreive the provided pickes using `git lfs`.
- Launch the notebook [exploit_res.ipynb](./exploit_res.ipynb) to extract the results that are shown in the paper. I left the output in that notebook so you can directly visualize the results I got from gitlab.

## Install / Upgrade

You can install / upgrade this code using the following command:

```console
pip install mapdiag --index-url https://gitlab.utc.fr/api/v4/projects/14147/packages/pypi/simple
```

## Documentation

The documentation of the package can be found [here](https://rhuet.gitlab.utc.fr/itsc2024/).
