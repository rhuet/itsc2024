Module documentation
====================

.. automodule:: mapdiag
   :members:
   :imported-members:
   :inherited-members:
   :special-members: __init__
   :undoc-members:
   :show-inheritance:
