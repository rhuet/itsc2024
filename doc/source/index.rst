.. mapdiag documentation master file, created by
   sphinx-quickstart on Tue Apr 16 18:05:00 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mapdiag's documentation!
===================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ./module.rst
   ./mapdiag.world.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
