mapdiag.world module
====================

.. automodule:: mapdiag.world
   :members:
   :imported-members:
   :inherited-members:
   :undoc-members:
   :show-inheritance:
